const hre = require("hardhat");
async function main() {
  const Wordle = await hre.ethers.getContractFactory("Wordle");
  const wordle = await Wordle.deploy();

  await wordle.deployed();

  console.log("Wordle deployed to ", wordle.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
