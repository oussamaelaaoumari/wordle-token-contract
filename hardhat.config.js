/**
 * @type import('hardhat/config').HardhatUserConfig
 */
require("@nomiclabs/hardhat-waffle");

const INFURA_URL = 'https://rinkeby.infura.io/v3/674a05a4ea6c4e32ac38ae1da70e1dc9';
const PRIVATE_KEY = '9bb191ef70ecc8c8e6099389e5865b35c1feaa0ac10d1e3ad5b46c5b48d65b23'


module.exports = {
  solidity: "0.8.2",
  networks: {
    hardhat : {
      chainId: 1337,
    }
  }
};
