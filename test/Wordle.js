
const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("test Wordle - play to earn game", () => {
  describe("Deployment", () => {
    it("Should Generate a word", async () => {
        const WORDLE = await ethers.getContractFactory("Wordle");
        const wordle = await WORDLE.deploy();
        await wordle.deployed();
        wordle.play();
        const generated_word = await wordle.wordle();
        console.log(generated_word);
        console.log(await wordle.checkWorlde("FOCAL")); 
        wordle.GUESS("EVADE");
        wordle.GUESS("DWARF");
        wordle.GUESS("DWARF");
        wordle.GUESS("DWARF");
        wordle.GUESS("DWARF");
        wordle.GUESS("FOCAL");
        console.log(generated_word);
        console.log(await wordle.playerStatistics("0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266" , 1));
        console.log(await wordle.TotalPlayerGames("0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266",1));
        console.log(await wordle.playerRecentWordle("0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266",1));
        console.log(await wordle.playerRecentWordleTries("0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266",1));
    });
  });
});