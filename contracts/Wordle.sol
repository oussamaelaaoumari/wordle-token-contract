// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "hardhat/console.sol";


contract Wordle is Ownable {
    string public wordle;
    string[5] public wordle_bytes = ["", "", "", "", ""];
    string[] public wordleList = [
        "CIGAR",
        "REBUT",
        "SISSY",
        "HUMPH",
        "AWAKE",
        "BLUSH",
        "FOCAL",
        "EVADE",
        "NAVAL",
        "SERVE",
        "HEATH",
        "DWARF",
        "MODEL",
        "KARMA"
    ];
    enum charState {
        RightPlace,
        WrongPlace,
        NotExist
    }
    enum gameStatus {
        PLAYING,
        WIN,
        LOSE
    }
    mapping(address => mapping(uint256 => gameStatus)) public playerStatistics;
    mapping(address => uint256) public TotalPlayerGames;
    mapping(address => mapping(uint256 => string)) public playerRecentWordle;
    mapping(address => mapping(uint256 => uint256)) public playerRecentWordleTries;
    WordleToken public WRDL;


    constructor(WordleToken _wordleTokenAddress) {
        WRDL = _wordleTokenAddress;
    }

    function play() public {
       // wordle = "FOCAL";
        require( WRDL.balanceOf(msg.sender) >= 1, "You should have one wordle token !");
        
        // calling WRDL.approve(address(this)) necessary in the frontend.
        WRDL.transferFrom( msg.sender, address(this) , 1);
    
        wordle = wordleList[random()];
        wordle_bytes = stringToBytes(wordle);
        playerRecentWordleTries[msg.sender][TotalPlayerGames[msg.sender]] = 0;
        playerStatistics[msg.sender][TotalPlayerGames[msg.sender]] = gameStatus.PLAYING;
        TotalPlayerGames[msg.sender] += 1;
    }

    function wordleToBytes(string memory _str) public {
        bytes memory str = bytes(_str);
        for (uint256 i = 0; i < 5; i++) {
            bytes memory _reverse = bytes(new string(1));
            _reverse[0] = str[i];
            wordle_bytes[i] = string(_reverse);
        }
    }

    function stringToBytes(string memory _str)
        private
        pure
        returns (string[5] memory)
    {
        bytes memory str = bytes(_str);
        string[5] memory StrWords = ["", "", "", "", ""];
        for (uint256 i = 0; i < 5; i++) {
            bytes memory _reverse = bytes(new string(1));
            _reverse[0] = str[i];
            StrWords[i] = string(_reverse);
        }
        return StrWords;
    }

    function guessToBytes(string memory _str)
        public
        pure
        returns (string[5] memory)
    {
        string[5] memory GUESS_bytes = ["", "", "", "", ""];
        bytes memory str = bytes(_str);
        for (uint256 i = 0; i < 5; i++) {
            bytes memory _reverse = bytes(new string(1));
            _reverse[0] = str[i];
            GUESS_bytes[i] = string(_reverse);
        }
        return GUESS_bytes;
    }

    function stop() public {
        playerStatistics[msg.sender][TotalPlayerGames[msg.sender]] = gameStatus.LOSE;
    }

    function random() private view returns (uint256) {
        return
            uint256(
                keccak256(abi.encodePacked(block.timestamp, block.difficulty))
            ) % wordleList.length;
    }

    function checkWordInList(string calldata guess)
        private
        view
        returns (bool)
    {
        for (uint256 i = 0; i < 15; i++) {
            if (
                keccak256(abi.encodePacked(wordleList[i])) ==
                keccak256(abi.encodePacked(guess))
            ) return true;
        }
        return false;
    }

    function checkWorlde(string calldata guess)
        public
        view
        returns (charState[5] memory)
    {
        require(bytes(guess).length == 5, "You should Enter 5 characters !");
        require(checkWordInList(guess), "You must write a valid word !!");
        charState[5] memory guessStatus;
        string[5] memory GUESS_bytes = ["", "", "", "", ""];
        GUESS_bytes = stringToBytes(guess);
        for (uint256 i = 0; i < 5; i++) {
            guessStatus[i] = checkCharStatus(GUESS_bytes[i], i);
        }
        return guessStatus;
    }

    function GUESS(string calldata guess) public returns (charState[5] memory) {
        require(
            playerRecentWordleTries[msg.sender][TotalPlayerGames[msg.sender]] <
                6,
            "You exceed the limit of tries in one word!!"
        );
        require(
            playerStatistics[msg.sender][TotalPlayerGames[msg.sender]] ==
                gameStatus.PLAYING,
            "You can't Play !"
        );
        playerRecentWordleTries[msg.sender][TotalPlayerGames[msg.sender]] += 1;
        charState[5] memory guessStatus = checkWorlde(guess);
        
        playerRecentWordleTries[msg.sender][
            TotalPlayerGames[msg.sender]
        ] = playerRecentWordleTries[msg.sender][TotalPlayerGames[msg.sender]];
        if (
            guessStatus[0] == charState.RightPlace &&
            guessStatus[1] == charState.RightPlace &&
            guessStatus[2] == charState.RightPlace &&
            guessStatus[3] == charState.RightPlace
        ) {
            playerStatistics[msg.sender][
                TotalPlayerGames[msg.sender]
            ] = gameStatus.WIN;
            playerRecentWordle[msg.sender][
                TotalPlayerGames[msg.sender]
            ] = wordle;
            WRDL.approve( address(this) , 2);
            WRDL.transferFrom( address(this), msg.sender , 2);
        } else if (
            playerRecentWordleTries[msg.sender][TotalPlayerGames[msg.sender]] ==6
        ) {
            playerStatistics[msg.sender][TotalPlayerGames[msg.sender]] = gameStatus.LOSE;
            playerRecentWordle[msg.sender][TotalPlayerGames[msg.sender]] = wordle;
        }
        return guessStatus;
    }

    function checkCharStatus(string memory guess_char, uint256 position)
        private
        view
        returns (charState)
    {
        for (uint256 i = 0; i < 5; i++) {
            if (
                keccak256(abi.encodePacked(guess_char)) ==
                keccak256(abi.encodePacked(wordle_bytes[i]))
            ) {
                if (position == i) return charState.RightPlace;
                else return charState.WrongPlace;
            }
        }
        return charState.NotExist;
    }
}

contract WordleToken is ERC20 {
    address private owner;
    constructor () ERC20("Wordle Token", "WRDL") {
        _mint(_msgSender(), 10000 * (10 ** uint256(decimals())));
        owner = msg.sender;
    }

    function getOwner() public view returns(address){
        return owner;
    }
}