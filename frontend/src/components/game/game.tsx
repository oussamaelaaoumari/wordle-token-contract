import { useState } from "react";
import GameRow from "../game-row/game-row";
import Header from "../header/header";
import Modal from "../modal/modal";
import "./game.scss";
const Game = () => {
  const [showModal, setShowModal] = useState(false);

  const handleClose = () => {
    setShowModal(false);
  };
  const handleShowModal = () => {
    setShowModal(true);
  };

  return (
    <div id="game">
      <Header handleShowModal={handleShowModal} />
      <GameRow />
      <Modal show={showModal} handleClose={handleClose}>
        <div>
          <p>WORDLE TOKEN - PLAY TO EARN GAME</p>
          <p>Invest One Token and win 2 Tokens</p>
          <p>You can Buy Tokens to play in this link</p>
          <p>Here is a video about how this game works.</p>
        </div>
      </Modal>
    </div>
  );
};
export default Game;
